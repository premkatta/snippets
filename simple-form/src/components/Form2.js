import React from 'react';
import '../css/Form2.css';

class Form2 extends React.Component{
  render(){
    return(
      <div className='formMain'>
      <div>
        <form>
          <div className='title'>
            <h1>Register</h1>
            <h5>Please fill your information below</h5>
          </div> 
        <div className='fields'>
          <div className='userName'>
            <input className='line-input' type='text' placeholder='Name' />
          </div>
          <div className='email'>
            <input className='line-input' type='email' placeholder='Email'/>
          </div>
          <div className='gender radiobox'>
            <label>Gender</label>
            <input type='radio' name='gender'/><a>Male</a>
            <input type='radio' name='gender'/><a>Female</a>
          </div>
          <div className='Type radiobox'>
            <label>Type</label>
            <input type='radio' name='type'/><a>Sponsered</a>
            <input type='radio' name='type'/><a>Paid</a>
          </div>
          <div className='address'>
            <input className='line-input' type='text' placeholder='Address'/>
          </div>
          <div className='State'>
            <input className='line-input' type='text' placeholder='State'/>
          </div>
          <div className='Phone'>
            <input className='line-input' type='tel' placeholder='Phone' pattern='[0-9]{10}'/>
          </div>
          <div className='submit'>
            <button type='submit'>Submit</button>
          </div>
        </div>
        </form>
        <div className='login'>
          <p>Already have an account? <a>Login here</a></p>
        </div>
      </div>
    </div>
    );
  }
}

export default Form2;