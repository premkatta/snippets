import React from 'react';
import '../css/Form1.css';





class Form1 extends React.Component{
  constructor(){
    super();
    this.state={
      states:['Andhrapradesh','MadyaPradesh','Bihar','Telangana','Tamilnadu','Kerala'],
      form:{
        email:null,
        password:null,
        firstname:null,
        lastname:null,
        gender:null,
        state:null
      },
    }; 
  }

  stateList(){
    return this.state.states.map(state=>{
    return (
        <option key={state}>
          {state}
        </option>
      );
    });
  }

  onFormSubmit=(event)=>{
    event.preventDefault();
    console.log(this.state.form);

    // this[this.state.firstname] = new this.User(this.state.firstname);
    // console.log(this.state.firstname);
  }

  render(){
    return(
      <div className='formMain'>
        <div className='title'>
          <div><h2>Registration Form</h2></div>
        </div>
        <div className='fields'>
         <form onSubmit={this.onFormSubmit}>
          <div className='email icon-and-input'>
            <div className='iconDiv'>
              <i className=' small middle aligned icon envelope'/>
            </div>
            <div className='input'>
              <input type='email' placeholder='Email' />
            </div>
          </div>
          <div className='password icon-and-input'>
            <div className='iconDiv'>
              <i className=' small middle aligned icon lock'/>
            </div>
            <div className='input'>
              <input type='password' placeholder='Password' />
            </div>
          </div>
          <div className='password icon-and-input'>
            <div className='iconDiv'>
              <i className=' small middle aligned icon lock'/>
            </div>
            <div className='input'>
              <input type='password' placeholder='Re-type Password' />
            </div>
          </div>
          <div className='first-lastName'>
            <div className='first-name icon-and-input'>
              <div className='iconDiv'>
                <i className=' small middle aligned icon user'/>
              </div>
              <div className='input'>
                <input type='text' placeholder='First Name' onChange={e=>this.setState({...this.state.form.firstname, firstname:e.target.value})} />
              </div>
            </div>
            <div className='last-name icon-and-input'>
              <div className='iconDiv'>
                <i className=' small middle aligned icon user'/>
              </div>
              <div className='input'>
                <input type='text' placeholder='Last Name' />
              </div>
            </div>
          </div>
          <div className='gender'>
            <input type='radio' name='gender' onChange={e=>{this.setState({gender:e.target.value})}}/> <a >Male</a>
            <input type='radio' name='gender' onChange={e=>{this.setState({gender:e.target.value})}}/> <a >Female</a>
          </div>
          <div className='state'>
            <select defaultValue='Select a state'>
              <option  disabled='disabled'>Select a state</option>
              {this.stateList()}
            </select>
          </div>
          <div className='terms'>
            <div><input type='checkbox' /> <a >I agree with terms and conditions</a></div>
            <div><input type='checkbox' /> <a >I want to receive the newsletter</a></div>
          </div>
          <div className='submitDiv'>
            {/* <div className='submit'>Submit</div> */}
            <button type='submit'>SUBMIT</button>
          </div>
         </form>
        </div>
      </div>
    );
  }
}

export default Form1;