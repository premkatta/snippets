import React from 'react';
import Form1 from './Form1';
// import Form2 from './Form2';
import '../css/App.css';

class App extends React.Component{
  render(){
    return(
      <div className='appMain'>
        <Form1 />
        {/* <Form2 /> */}
      </div>
    );
  }
}

export default App;