import React from 'react';
import './App.css';
import Home from './components/Home.js';

function App() {
  return (
    <div className="App">
      <p>Something is written here!!</p>
      <Home></Home>
    </div>
  );
}

export default App;
