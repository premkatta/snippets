import React from 'react';
import axios from 'axios';

class Home extends React.Component{
  constructor(props){
    super(props);
    this.state={abc:[]}
  }


  componentDidMount(){
    console.log('testing');
    axios.get('https://in.bookmyshow.com/serv/getData?cmd=QUICKBOOK&type=MT&getRecommendedData=1&_=1558610666623').then((res)=>{
      this.setState({abc:res.data});
      console.log(this.state.abc);
    })
  }
  render(){
    return(
      <div>
        <p>This is a Home Page!</p>
        <p>{this.state.abc.toString()}</p>
      </div>
    )
  }
}

export default Home;