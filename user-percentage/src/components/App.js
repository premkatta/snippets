import React from 'react';
import axios from 'axios';
import '../css/App.css';

class App extends React.Component{
  
  constructor(props){
    super(props);
    this.state={userData:null};
  }

  componentDidMount(){
    axios.get('https://my.api.mockaroo.com/user-fulldata.json?key=4595d050').then(response=>{
      this.setState({userData: response.data});
      console.log(this.state.userData);
    });
  }

  renderedData(){
    if(!this.state.userData){
      return null;
    }
    return this.state.userData.map(user=>{
      return (
        <tr key={user.id}>
          <td>{user.first_name}</td>
          <td>hello</td>
          <td>{user.Date}</td>
          <div className="progress">
            <div className="progress-bar" role="progressbar" style={{width: `${user.Percentage}%`}}  aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <td>{user.Percentage}%</td>
        </tr>
      );
    })
  }
  render(){
    return(
      <div className='App-main'>
        <div className='header'>
          <table class='table table-sm'>
            <thread>
              <tr>
                <td className='col-md-3'><i className="user icon"></i></td>
                <td scope='col'>Name</td>
                <td scope='col'>Date</td>
                <td scope='col'>Participation</td>
                <td scope='col'>Percentage</td>
              </tr>
            </thread>
            <tbody>
              {this.renderedData()}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default App;