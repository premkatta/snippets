import React from 'react';

class Home extends React.Component{
	constructor(){
		super();
			this.state={
					userName:'premkatta',
					count:1,
			}
	}
	changeName = ()=>{
		this.setState({userName:'premkumar'});
	}
	userNameChange = (event)=>{
		event.preventDefault();
		// this.setState({userName:event.target.value});
	}
	focusChange = ()=>{
		this.setState({count:this.state.count+1});
		console.log(this.state.count);
	}
	render(){
		return(
			<div>
				<form onSubmit={this.userNameChange}>
					<p>{this.state.userName}</p>
					{/* <button onClick={this.changeName}>Change Name</button> */}
					<input type = 'password' placeholder='enter password' onFocus={this.focusChange} />
					<input type='text' placeholder='enter the name' onChange={e=>{this.setState({userName:e.target.value})}}/>
				</form>
			</div>

		);
	}
}

export default Home;