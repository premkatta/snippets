import React from 'react';
import logo from '../images/logo.svg';
import '../css/App.css';

class App extends React.Component{
	render(){
		return(
			<div className='app-main'>
				<header className='header-main'>
					<img className='app-logo' src={logo} alt='logo' />
					<p>Refer here for <code>Code</code></p>
					<a href='https://reactjs.org' target='_blank' rel='noopener noreferrer'>reactjs.org</a>
				</header>
			</div>
		);
	}
}

export default App;