import React from 'react';
import { connect } from 'react-redux';
import { fetchUsers, selectUser } from '../actions';
import '../css/UserList.css';

class UserList extends React.Component{
  componentDidMount(){
    this.props.fetchUsers();
  }

  renderedUserList = ()=>{
    if(this.props.users.length===0){
      return null;
    }
    else{
    return this.props.users[0].map(user=>{
      return(
        <div className='user-name' key={user.id}>
          <p onClick={()=>this.props.selectUser(user)}>{user.first_name}</p>
        </div>
      );
    });
  }
  }

  renderedSelectedUser = ()=>{
    let selectedUser = this.props.selectedUser;
    if(selectedUser.length===0){
      return (
        <div>
          Please select an user.
        </div>
      );
    }
    return (
      <div className='user-details'>
        <div className='user-icon'>
          <img src={selectedUser.User_icon} alt={selectedUser.first_name} />
        </div>
        <div className='user-info'>
          <p>{selectedUser.first_name}</p>
          <p>{selectedUser.last_name}</p>
          <p>{selectedUser.gender}</p>
          <p>{selectedUser.email}</p>
        </div>
      </div>
    );
  };

  render(){
    return(
      <div>
        <div className='user-list-panel'>
          {this.renderedUserList()}
        </div>
        <div className='user-details-panel'>
            {this.renderedSelectedUser()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state)=>{
  console.log(state, new Date().getSeconds());
  return ({
    users : state.usersData,
    selectedUser : state.selectedUser
  });
}

export default connect(mapStateToProps, { fetchUsers, selectUser })(UserList);
