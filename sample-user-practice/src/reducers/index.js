import { combineReducers } from 'redux';
import usersDataReducer from './usersDataReducer';
import selectedUserReducer from './selectedUserReducer';

export default combineReducers({
  usersData : usersDataReducer,
  selectedUser : selectedUserReducer
});