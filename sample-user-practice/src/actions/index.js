import UsersData from '../apis/UsersData';

export const fetchUsers = ()=>{
  return async function (dispatch){
    const response = await UsersData.get();
    // console.log("response",response);
    dispatch({type:'FETCH_USER', payload:response.data});
  }
};

export const selectUser = (user)=>{
    return {
      type:'SELECTED_USER', payload:user
    };
};
