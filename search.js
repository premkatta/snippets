class User{
  constructor(name,age){
    this.name=name;
    this.age=age;
  }
}

function createUser(name, age){
  this[name] = new User(name, age);
}

function search(arr, key){
  for(let i=0;i<arr.length;i++){
    if(arr[i].name === key) return arr[i];
  }
  return 'Key not found';
}